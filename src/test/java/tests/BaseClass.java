package tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.*;
import pages.*;

import java.io.FileInputStream;
import java.io.IOException;
import java.time.Duration;
import java.util.Properties;

public class BaseClass {
    WebDriver driver;
    Properties prop;
    IndexPage indexPage;
    ProductListingPage productListingPage;
    ProductInfoPage productInfoPage;
    CartPage cartPage;
    LoginPage loginPage;
    LogoutPage logoutPage;

    @BeforeMethod
    public void startup() throws IOException {
        prop = new Properties();
        prop.load(new FileInputStream("src/main/resources/Config.properties"));
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        driver.get(prop.getProperty("url"));

        indexPage = new IndexPage(driver);
        productListingPage = new ProductListingPage(driver);
        productInfoPage = new ProductInfoPage(driver);
        cartPage = new CartPage(driver);
        loginPage = new LoginPage(driver);
        logoutPage = new LogoutPage(driver);
    }
    @Test
    public void testAmazonEndToEnd() throws InterruptedException {
        indexPage.search("t-shirts");
        Thread.sleep(2000);
        productListingPage.clickOnProduct();
        Thread.sleep(2000);
        productListingPage.switchToNewWindow();
        Thread.sleep(2000);

        productInfoPage.clickOnAddToCartBtn();
        Thread.sleep(2000);
        productInfoPage.clickOnGoToCartBtn();
        Thread.sleep(2000);

        cartPage.validateProceedToBuy();
        Thread.sleep(2000);
        loginPage.enterMobile(prop.getProperty("mobileNo"));
        Thread.sleep(2000);
        loginPage.enterPassword(prop.getProperty("password"));
        Thread.sleep(2000);
        loginPage.returnHomePage();

        logoutPage.mouseHover();
        Thread.sleep(2000);
        logoutPage.verifySignOut();
        Assert.assertEquals(driver.getTitle(),"Amazon Sign In");
    }
    @AfterMethod
    public void lastStep(){
        driver.quit();
    }
}
