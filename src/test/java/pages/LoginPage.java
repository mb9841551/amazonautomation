package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {
    WebDriver driver;

    @FindBy(id = "ap_email")
    WebElement mobileKey;
    @FindBy(id = "continue")
    WebElement continueBtn;
    @FindBy(id = "ap_password")
    WebElement passwordKey;
    @FindBy(id = "signInSubmit")
    WebElement signInBtn;
    @FindBy (xpath = "//a[contains(text(),'Amazon.in homepage')]")
    WebElement goToHomPageBtn;
    @FindBy(xpath = "//h1[contains(text(),'Checkout')]")
    WebElement checkoutText;

    public LoginPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void enterMobile(String mobileNo){
        try{
            mobileKey.sendKeys(mobileNo);
            continueBtn.click();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
    public void enterPassword(String password){
        try{
            passwordKey.sendKeys(password);
            signInBtn.click();
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

    public WebElement validateCheckoutText(){
        return checkoutText;
    }

    public void returnHomePage(){
        goToHomPageBtn.click();
    }



}
