package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ProductInfoPage {
    WebDriver driver;

    @FindBy(xpath = "//input[@id='add-to-cart-button']")
    WebElement addToCart;

    @FindBy(xpath = "//span[@id='sw-gtc']//a[@class='a-button-text']")
    WebElement goToCart;

    @FindBy(xpath = "//h1[contains(text(),'Added to Cart')]")
    WebElement addedToCartText;

    public ProductInfoPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void clickOnAddToCartBtn(){
        addToCart.click();
    }

    public WebElement checkGoToCartPage(){
        return addedToCartText;
    }

    public void clickOnGoToCartBtn(){
        goToCart.click();
    }

}
