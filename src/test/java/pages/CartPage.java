package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CartPage {
    WebDriver driver;

    @FindBy(xpath = "//h1[contains(text(),'Shopping Cart')]")
    WebElement shoppingCartText;

    @FindBy(xpath = "//input[@name='proceedToRetailCheckout']")
    WebElement proceedToBuyBtn;

    public CartPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }
    public void validateProceedToBuy(){
        proceedToBuyBtn.click();
    }
    public WebElement verifyShoppingCartText(){
        return shoppingCartText;
    }
}
