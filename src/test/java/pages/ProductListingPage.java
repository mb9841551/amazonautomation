package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.Set;

public class ProductListingPage {
    WebDriver driver;

    @FindBy(xpath = "(//div[@class='a-section aok-relative s-image-tall-aspect'])[2]")
    WebElement product;

    @FindBy(xpath = "//img[@class='nav-categ-image']")
    WebElement amazonFashionText;

    public ProductListingPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void clickOnProduct(){
        product.click();
    }

    public WebElement validateProduct(){
        return amazonFashionText;
    }

    public void switchToNewWindow(){
        String currentWindowHandle = driver.getWindowHandle();
        Set<String> windowHandles = driver.getWindowHandles();
        for(String windowHandle:windowHandles){
            if(!windowHandle.equals(currentWindowHandle)){
                driver.switchTo().window(windowHandle);
                break;
            }
        }
    }
}
