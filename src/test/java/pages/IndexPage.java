package pages;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class IndexPage {
    WebDriver driver;

    @FindBy(id="twotabsearchtextbox")
    WebElement searchbox;

    @FindBy(id="nav-search-submit-button")
    WebElement searchBtn;

//    @FindBy(id="nav-link-accountList")
//    WebElement signinBtn;

    public IndexPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public String getAmazonTitle(){
        String amazonTitle;
        amazonTitle = driver.getTitle();
        return amazonTitle;
    }

//    public LoginPage clickOnSignIn(){
//        signinBtn.click();
//        return new LoginPage();
//    }

    public boolean search(String item){
        try{
            searchbox.sendKeys(item);
            searchBtn.click();
            return true;
        }
        catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

}
