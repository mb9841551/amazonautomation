package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LogoutPage {
    WebDriver driver;
    Actions builder;

    @FindBy(id = "nav-link-accountList")
    WebElement accountAndListArea;
    @FindBy(xpath = "//span[text()='Sign Out']")
    WebElement signOutBtn;

    public LogoutPage(WebDriver driver){
        this.driver = driver;
        this.builder = new Actions(driver);
        PageFactory.initElements(driver, this);
    }

    public void mouseHover(){
        builder.moveToElement(accountAndListArea).build().perform();
    }
    public void verifySignOut(){
        builder.moveToElement(signOutBtn).build().perform();
        signOutBtn.click();
    }



}
